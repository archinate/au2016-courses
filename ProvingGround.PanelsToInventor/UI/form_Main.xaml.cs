﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Autodesk.Revit;
using Autodesk.Revit.DB;

using ProvingGround.PanelsToInventor.Classes;

namespace ProvingGround.PanelsToInventor
{
    /// <summary>
    /// Interaction logic for form_Main.xaml
    /// </summary>
    public partial class form_Main : Window
    {
        private RevitCommandHelper _rvtCmd;
        private string _dir = "";

        public form_Main(RevitCommandHelper cmdDataHelper)
        {
            InitializeComponent();

            //widen scope
            _rvtCmd = cmdDataHelper;
        }


        /// <summary>
        /// Close the window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Close_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Run code here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Run_Click(object sender, RoutedEventArgs e)
        {
            if (!System.IO.Directory.Exists(_dir))
            {
                MessageBox.Show("Please select a valid directory.");
            }
            else
            {

                double m_flangeDepth = 0;
                try
                {
                    m_flangeDepth = Convert.ToDouble(this.TextBox_FlangeDepth.Text);
                }
                catch
                {
                    
                }

                string symbName = "QuadAdaptive-Simple";
                FamilySymbol fs = GetFamilySymbol(symbName);

                if (fs != null)
                {

                    List<List<XYZ>> panelPoints = new List<List<XYZ>>();

                    var familyInstances = GetFamilyInstances(fs);

                    foreach (FamilyInstance fi in familyInstances)
                    {

                        IList<ElementId> adptPts = AdaptiveComponentInstanceUtils.GetInstancePlacementPointElementRefIds(fi);

                        var pPts = new List<XYZ>();

                        foreach (ElementId eId in adptPts)
                        {
                            ReferencePoint thisRef = (ReferencePoint)_rvtCmd.ActiveDoc.GetElement(eId);
                            pPts.Add(new XYZ(thisRef.Position.X, thisRef.Position.Y, thisRef.Position.Z));
                        }

                        panelPoints.Add(pPts);
                    }

                    try
                    {
                        InventorPanel Exporter = new InventorPanel((Inventor.Application)
                    System.Runtime.InteropServices.Marshal.
                    GetActiveObject("Inventor.Application"), "ft");

                        Exporter.CollectAndExport(panelPoints, _dir, "Panel", m_flangeDepth);
                    }
                    catch
                    {

                        MessageBox.Show("Couldn't communicate with Inventor!");
                    }

                }
                else
                {
                    MessageBox.Show("The QuadAdaptive-Simple does not exist in this document.");
                }
            }
            
        }

        /// <summary>
        /// Drag Window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        #region private methods


        /// <summary>
        /// Get FamilySymbol
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private FamilySymbol GetFamilySymbol(string name)
        {
            FamilySymbol fs = null;
            FilteredElementCollector filtElem = new FilteredElementCollector(_rvtCmd.ActiveDoc);
            filtElem.OfClass(typeof(FamilySymbol));
            foreach (FamilySymbol f in filtElem)
            {
                if (f.Name == name)
                {
                    fs = f;
                    break;
                }
            }

            return fs;
        }

        private List<FamilyInstance> GetFamilyInstances(FamilySymbol fs)
        {

            List<FamilyInstance> famInstances = new List<FamilyInstance>();

            FilteredElementCollector filtElem = new FilteredElementCollector(_rvtCmd.ActiveDoc);
            filtElem.OfClass(typeof(FamilyInstance));

            foreach (FamilyInstance fi in filtElem)
            {
                if (fi.Symbol.Id == fs.Id)
                {
                    famInstances.Add(fi);
                }
            }

            return famInstances;
        }

        private string FolderBrowser()
        {
            System.Windows.Forms.FolderBrowserDialog m_folderbrowser = new System.Windows.Forms.FolderBrowserDialog();
            System.Windows.Forms.DialogResult result = m_folderbrowser.ShowDialog();
            if (m_folderbrowser.SelectedPath != null)
            {
                return m_folderbrowser.SelectedPath;
            }
            else
            {
                return null;
            }
        }

        #endregion

        private void Button_GetDirectory_Click(object sender, RoutedEventArgs e)
        {
            string dir = FolderBrowser();
            _dir = dir;
            TextBox_Directory.Text = dir;
        }
    }
}
