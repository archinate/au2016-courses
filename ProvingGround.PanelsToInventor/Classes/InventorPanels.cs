﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Autodesk.Revit;
using Autodesk.Revit.DB;

namespace ProvingGround.PanelsToInventor.Classes
{

    public class InventorPanel
    {

        public Inventor.Application App { get; set; }
        public Inventor.TransientGeometry Trans { get; set; }
        public Inventor.PartDocument Doc { get; set; }
        public Inventor.SheetMetalComponentDefinition CompDef { get; set; }
        public Inventor.UnitsOfMeasure Units { get; set; }
        public string RevitUnits { get; set; }

        public InventorPanel(Inventor.Application thisApp, string revitUnits)
        {
            App = thisApp;
            Trans = App.TransientGeometry;
            RevitUnits = revitUnits;
        }

        public void CollectAndExport(List<List<XYZ>> RevitPoints, string InvDirectory, string InvFileName, double FlangeDepth)
        {

          // create the underlying geometry from collection of polylines;

          InventorPanel RVI = new InventorPanel((Inventor.Application)
            System.Runtime.InteropServices.Marshal.
            GetActiveObject("Inventor.Application"), "ft");

          RVI.App.ScreenUpdating = false;
          RVI.App.UserInterfaceManager.UserInteractionDisabled = true;

          Inventor.ProgressBar oProgressBar = RVI.App.CreateProgressBar(false, RevitPoints.Count, "Import Panels from Grasshopper");

          for (int i = 0; i < RevitPoints.Count; i++)
          {

            oProgressBar.Message = "Panel " + (i + 1).ToString();
            oProgressBar.UpdateProgress();

            //
            RVI.Doc = (Inventor.PartDocument) RVI.App.Documents.Add(Inventor.DocumentTypeEnum.kPartDocumentObject,
              RVI.App.FileManager.GetTemplateFile(Inventor.DocumentTypeEnum.kPartDocumentObject,
              Inventor.SystemOfMeasureEnum.kDefaultSystemOfMeasure, Inventor.DraftingStandardEnum.kDefault_DraftingStandard,
              "{9C464203-9BAE-11D3-8BAD-0060B0CE6BB4}"), true);

            RVI.CompDef = (Inventor.SheetMetalComponentDefinition) RVI.Doc.ComponentDefinition;
            RVI.Units = RVI.Doc.UnitsOfMeasure;

            Inventor.WorkPoint[] m_worker = new Inventor.WorkPoint[RevitPoints[i].Count];

            for (int j = 0; j < RevitPoints[i].Count; j++)
            {
                m_worker[j] = RVI.WorkPoint(RevitPoints[i][j]);
            }

            Inventor.Vector m_xAxis = m_worker[0].Point.VectorTo(m_worker[1].Point);
            Inventor.Vector m_yAxis = m_worker[0].Point.VectorTo(m_worker[2].Point);

            Inventor.WorkPlane m_thisPlane = 
                RVI.CompDef.WorkPlanes.AddFixed(m_worker[0].Point,
                  m_xAxis.AsUnitVector(),
                  m_yAxis.AsUnitVector(), true);


            Inventor.PlanarSketch m_sketch2D = 
                RVI.Doc.ComponentDefinition.Sketches.Add(m_thisPlane);

            //m_sketch2D = oCompDef.Sketches.Add(m_workPlane);

            for (int j = 0; j < m_worker.Length; j++)
            {
                int next = (j + 1) % RevitPoints[i].Count;
                Inventor.SketchLine m_thisEdge = 
                    RVI.SketchLine(RevitPoints[i][j], RevitPoints[i][next], ref m_sketch2D);
            }

            RVI.CombineSketchLines(ref m_sketch2D);

            Inventor.Profile oProfile = m_sketch2D.Profiles.AddForSolid();

            Inventor.SheetMetalFeatures oSheetMetalFeatures =
              (Inventor.SheetMetalFeatures) RVI.CompDef.Features;

            Inventor.FaceFeatureDefinition oFaceFeatureDefinition = 
                oSheetMetalFeatures.FaceFeatures.CreateFaceFeatureDefinition(oProfile);

            // Create a face feature.
            Inventor.FaceFeature oFaceFeature = oSheetMetalFeatures.FaceFeatures.Add(oFaceFeatureDefinition);

            // Get the top face for creating the new sketch.
            // We'll assume that the 6th face is the top face.
            Inventor.Face oFrontFace = oFaceFeature.Faces[oFaceFeature.Faces.Count];

            // Create a collection containing all edges.
            Inventor.EdgeCollection edgeSet = RVI.App.TransientObjects.CreateEdgeCollection();
            for (int j = 0; j < RevitPoints[i].Count; j++)
            {
              edgeSet.Add(oFrontFace.Edges[j + 1]);
            }

              // If a flange depth is specified, add flanges to sheet
            if (FlangeDepth > 0)
            {
                Inventor.FlangeDefinition flangeDef =
              oSheetMetalFeatures.FlangeFeatures.CreateFlangeDefinition(edgeSet, "90", RVI.Convert(FlangeDepth / 12.0));

                oSheetMetalFeatures.FlangeFeatures.Add(flangeDef);
            }

            RVI.Doc.SaveAs(InvDirectory + @"\Panel " + i.ToString() + ".ipt", false);
            RVI.Doc.Close(true);

          }

          oProgressBar.Close();

          RVI.App.ScreenUpdating = true;
          RVI.App.UserInterfaceManager.UserInteractionDisabled = false;

          // Set a reference to the active assembly document.
          // This assumes an assembly document is open.
          Inventor.AssemblyDocument oDoc = (Inventor.AssemblyDocument) RVI.App.ActiveDocument;

          // Set a reference to the assembly component definition.
          Inventor.AssemblyComponentDefinition oAsmCompDef = oDoc.ComponentDefinition;

          // Create a matrix.  A new matrix is initialized with an identity matrix.
          Inventor.Matrix oMatrix = RVI.Trans.CreateMatrix();

          for (int i = 0; i < RevitPoints.Count; i++)
          {
            // Add the occurrence.
              Inventor.ComponentOccurrence oOcc = 
                  oAsmCompDef.Occurrences.Add(InvDirectory + @"\Panel " + i.ToString() + ".ipt", oMatrix);
          }

        }


        /// <summary>
        /// Convert a unit from the revit model to the Inventor API internal setting ("cm")
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public double Convert(double value)
        {
            return this.Units.ConvertUnits(value, this.RevitUnits, "cm");
        }

        /// <summary>
        /// Create an Inventor work point from a revit point and add it to the component definition
        /// </summary>
        /// <param name="revitPoint"></param>
        /// <returns></returns>
        public Inventor.WorkPoint WorkPoint(XYZ revitPoint)
        {
            return this.CompDef.WorkPoints.AddFixed(this.Point(revitPoint));
        }

        /// <summary>
        /// Create a transient geometry point and add it to the application transient geometry
        /// </summary>
        /// <param name="revitPoint"></param>
        /// <returns></returns>
        public Inventor.Point Point(XYZ revitPoint)
        {
            return this.Trans.CreatePoint(
                this.Convert(revitPoint.X),
                this.Convert(revitPoint.Y),
                this.Convert(revitPoint.Z));
        }

        /// <summary>
        /// Add a sketch line to a planar sketch from two revit points
        /// </summary>
        /// <param name="revitStartPt"></param>
        /// <param name="revitEndPt"></param>
        /// <param name="sketch2D"></param>
        /// <returns></returns>
        public Inventor.SketchLine SketchLine(XYZ revitStartPt, XYZ revitEndPt, ref Inventor.PlanarSketch sketch2D)
        {
            return sketch2D.SketchLines.AddByTwoPoints(
                sketch2D.ModelToSketchSpace(this.Trans.CreatePoint(
                this.Convert(revitStartPt.X),
                this.Convert(revitStartPt.Y),
                this.Convert(revitStartPt.Z))),

                sketch2D.ModelToSketchSpace(this.Trans.CreatePoint(
                this.Convert(revitEndPt.X),
                this.Convert(revitEndPt.Y),
                this.Convert(revitEndPt.Z))));
        }

        /// <summary>
        /// Combine planar sketch lines
        /// </summary>
        /// <param name="sketch2D"></param>
        /// <returns></returns>
        public void CombineSketchLines(ref Inventor.PlanarSketch sketch2D)
        {
            for (int j = 1; j <= sketch2D.SketchLines.Count; j++)
            {
                int next = j < sketch2D.SketchLines.Count ? j + 1 : 1;
                sketch2D.SketchLines[next].StartSketchPoint.Merge(
                    sketch2D.SketchLines[j].EndSketchPoint);
            }
        }
    }

}
