# AU2016 - Revit API for Designers—Use Cases for Extending Creativity #

This is the dataset created as part of a Revit API course presented by Nathan Miller and David Stasiuk. The examples show basic workflows for creating Revit Add-in tools that can aid in the design process. Please note that these are only examples for educational purposes and should not be thought of as "production ready" add-ins. The source code is provided under the MIT license (see below).

### What do the example show? ###
Two main examples are provided in this dataset...

* **ProvingGround.PanelGeneration** demonstrates creating a basic add-in with a form for dividing Revit element faces into adaptive component panels. The example has an example of a WPF form that allows a user to pick a face from the Revit model and then set division settings.
* **ProvingGround.PanelsToInventor** demonstrates a basic workflow for taking adaptive component panels and generating Inventor-based sheet metal parts with flanges. The example shows how you can tap into model data and use the API to create new interoperability workflows.

![PanelGeneratorExample.jpg](https://bitbucket.org/repo/jz97A4/images/3443130243-PanelGeneratorExample.jpg)

### How do I get set up? ###

* Download the repository.
* Build the add-ins and run in Revit.
* Add-ins require Revit 2017.

### MIT License ###

Copyright (c) 2016 Proving Ground LLC

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.