﻿#region Namespaces
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
#endregion

namespace ProvingGround.RevitAddin20171
{
    [Transaction(TransactionMode.Manual)]
    public class cmd : IExternalCommand
    {
        public Result Execute(
          ExternalCommandData commandData,
          ref string message,
          ElementSet elements)
        {

            // Settings Class
            clsSettings m_settings = new clsSettings(commandData);

            // Load Form
            form_Main m_form = new form_Main(m_settings);
            m_form.ShowDialog();

            return Result.Succeeded;
        }
    }
}