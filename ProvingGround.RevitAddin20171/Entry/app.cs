﻿#region Namespaces
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI;
using System.Windows.Media.Imaging;
using System.IO;
using System.Reflection;
using System.Windows.Media;

using Autodesk.Revit.UI;
using Autodesk.Revit.DB.PointClouds;
#endregion

namespace ProvingGround.RevitAddin20171
{
    [Transaction(TransactionMode.Manual)]
    class app : IExternalApplication
    {
        static string m_Path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

        static UIControlledApplication m_uiApp;
        /// <summary>
        /// Load an Image Source from File
        /// </summary>
        /// <param name="SourceName"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        private ImageSource LoadPngImgSource(string SourceName)
        {


            try
            {
                // Assembly
                Assembly m_assembly = Assembly.GetExecutingAssembly();

                // Stream
                Stream m_icon = m_assembly.GetManifestResourceStream(SourceName);

                // Decoder
                PngBitmapDecoder m_decoder = new PngBitmapDecoder(m_icon, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);

                // Source
                ImageSource m_source = m_decoder.Frames[0];
                return (m_source);


            }
            catch
            {
            }

            // Fail
            return null;

        }

        /// <summary>
        /// Add the Ribbon Item and Panel
        /// </summary>
        /// <param name="a"></param>
        /// <remarks></remarks>
        public void AddRibbonPanel(UIControlledApplication a)
        {
            try
            {
                // First Create the Tab
                a.CreateRibbonTab("Proving Ground");
            }
            catch
            {
                // Might already exist...
            }

            // Tools
            AddButton("Proving Ground", "AU2016\r\nPlugin", "AU2016\r\nPlugin", "ProvingGround.RevitAddin20171.Template_16.png", "ProvingGround.RevitAddin20171.Template_32.png", (m_Path + "\\ProvingGround.RevitAddin20171.dll"), "ProvingGround.RevitAddin20171.cmd", "Template add-in for Revit 2017.");
        }

        /// <summary>
        /// Add a button to a Ribbon Tab
        /// </summary>
        /// <param name="Rpanel">The name of the ribbon panel</param>
        /// <param name="ButtonName">The Name of the Button</param>
        /// <param name="ButtonText">Command Text</param>
        /// <param name="ImagePath16">Small Image</param>
        /// <param name="ImagePath32">Large Image</param>
        /// <param name="dllPath">Path to the DLL file</param>
        /// <param name="dllClass">Full qualified class descriptor</param>
        /// <param name="Tooltip">Tooltip to add to the button</param>
        /// <returns></returns>
        /// <remarks></remarks>
        private bool AddButton(string Rpanel, string ButtonName, string ButtonText, string ImagePath16, string ImagePath32, string dllPath, string dllClass, string Tooltip)
        {
            try
            {
                // The Ribbon Panel
                RibbonPanel m_RibbonPanel = null;

                // Find the Panel within the Case Tab
                List<RibbonPanel> m_RP = new List<RibbonPanel>();
                m_RP = m_uiApp.GetRibbonPanels("Proving Ground");
                foreach (RibbonPanel x in m_RP)
                {
                    if (x.Name.ToUpper() == Rpanel.ToUpper())
                    {
                        m_RibbonPanel = x;
                    }
                }

                // Create the Panel if it doesn't Exist
                if (m_RibbonPanel == null)
                {
                    m_RibbonPanel = m_uiApp.CreateRibbonPanel("Proving Ground", Rpanel);
                }

                // Create the Pushbutton Data
                PushButtonData m_PushButtonData = new PushButtonData(ButtonName, ButtonText, dllPath, dllClass);
                if (!string.IsNullOrEmpty(ImagePath16))
                {
                    try
                    {
                        m_PushButtonData.Image = LoadPngImgSource(ImagePath16);
                    }
                    catch
                    {
                    }
                }
                if (!string.IsNullOrEmpty(ImagePath32))
                {
                    try
                    {
                        m_PushButtonData.LargeImage = LoadPngImgSource(ImagePath32);
                    }
                    catch
                    {
                    }
                }
                m_PushButtonData.ToolTip = Tooltip;

                // Add the button to the tab
                PushButton m_PushButtonData_Add = (PushButton)m_RibbonPanel.AddItem(m_PushButtonData);
            }
            catch
            {
                // Quiet Fail
            }
            return true;
        }
        public Result OnStartup(UIControlledApplication a)
        {
            try
            {
                // The Shared uiApp variable
                m_uiApp = a;
                // Add the Ribbon Panel!!
                AddRibbonPanel(a);

                // Return Success
                return Result.Succeeded;

            }
            catch { return Result.Failed; }

        }

        public Result OnShutdown(UIControlledApplication a)
        {
            return Result.Succeeded;
        }
    }
}