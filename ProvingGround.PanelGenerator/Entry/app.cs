﻿#region Namespaces
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.Attributes;
using System.Windows.Media.Imaging;
using System.IO;
using System.Reflection;
using System.Windows.Media;

using Autodesk.Revit.UI;
using Autodesk.Revit.DB.PointClouds;
#endregion

namespace ProvingGround.PanelGenerator
{
    [Transaction(TransactionMode.Manual)]
    class app : IExternalApplication
    {
        static string _path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

        static UIControlledApplication uiApp;
        /// <summary>
        /// Load an Image Source from File
        /// </summary>
        /// <param name="SourceName"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        private ImageSource LoadPngImgSource(string SourceName)
        {
            try
            {
                // Assembly
                Assembly assmb = Assembly.GetExecutingAssembly();

                // Stream
                Stream btnIcon = assmb.GetManifestResourceStream(SourceName);

                // Decoder
                PngBitmapDecoder m_decoder = new PngBitmapDecoder(btnIcon, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);

                // Source
                ImageSource imgSrc = m_decoder.Frames[0];
                return (imgSrc);


            }
            catch { }
            // Fail
            return null;
        }

        /// <summary>
        /// Add the Ribbon Item and Panel
        /// </summary>
        /// <param name="a"></param>
        /// <remarks></remarks>
        public void AddRibbonPanel(UIControlledApplication a)
        {
            try
            {
                // First Create the Tab
                a.CreateRibbonTab("Proving Ground");
            }
            catch
            {
                // Might already exist...
            }

            // Tools
            AddButton("AU2016", 
                "Panel\r\nGenerator", 
                "Panel\r\nGenerator", 
                "ProvingGround.PanelGenerator.Template_16.png", 
                "ProvingGround.PanelGenerator.Template_32.png", 
                (_path + "\\ProvingGround.PanelGenerator.dll"), 
                "ProvingGround.PanelGenerator.cmd", 
                "Create panel geometry with the Revit API.");
        }

        /// <summary>
        /// Add a button to a Ribbon Tab
        /// </summary>
        /// <param name="Rpanel">The name of the ribbon panel</param>
        /// <param name="ButtonName">The Name of the Button</param>
        /// <param name="ButtonText">Command Text</param>
        /// <param name="ImagePath16">Small Image</param>
        /// <param name="ImagePath32">Large Image</param>
        /// <param name="dllPath">Path to the DLL file</param>
        /// <param name="dllClass">Full qualified class descriptor</param>
        /// <param name="Tooltip">Tooltip to add to the button</param>
        /// <returns></returns>
        /// <remarks></remarks>
        private bool AddButton(string Rpanel, string ButtonName, string ButtonText, string ImagePath16, string ImagePath32, string dllPath, string dllClass, string Tooltip)
        {
            try
            {
                // The Ribbon Panel
                RibbonPanel ribbonPanel = null;

                // Find the Panel within the Case Tab
                List<RibbonPanel> m_RP = new List<RibbonPanel>();
                m_RP = uiApp.GetRibbonPanels("Proving Ground");
                foreach (RibbonPanel x in m_RP)
                {
                    if (x.Name.ToUpper() == Rpanel.ToUpper())
                    {
                        ribbonPanel = x;
                    }
                }

                // Create the Panel if it doesn't Exist
                if (ribbonPanel == null)
                {
                    ribbonPanel = uiApp.CreateRibbonPanel("Proving Ground", Rpanel);
                }

                // Create the Pushbutton Data
                PushButtonData pushButtonData = new PushButtonData(ButtonName, ButtonText, dllPath, dllClass);
                if (!string.IsNullOrEmpty(ImagePath16))
                {
                    try
                    {
                        pushButtonData.Image = LoadPngImgSource(ImagePath16);
                    }
                    catch
                    {
                    }
                }
                if (!string.IsNullOrEmpty(ImagePath32))
                {
                    try
                    {
                        pushButtonData.LargeImage = LoadPngImgSource(ImagePath32);
                    }
                    catch
                    {
                    }
                }
                pushButtonData.ToolTip = Tooltip;

                // Add the button to the tab
                PushButton pushButtonData_Add = (PushButton)ribbonPanel.AddItem(pushButtonData);
            }
            catch
            {
                // Quiet Fail
            }
            return true;
        }
        public Result OnStartup(UIControlledApplication a)
        {
            try
            {
                // The Shared uiApp variable
                uiApp = a;
                // Add the Ribbon Panel!!
                AddRibbonPanel(a);

                // Return Success
                return Result.Succeeded;

            }
            catch { return Result.Failed; }

        }

        public Result OnShutdown(UIControlledApplication a)
        {
            return Result.Succeeded;
        }
    }
}