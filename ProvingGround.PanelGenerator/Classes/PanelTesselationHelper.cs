﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Autodesk.Revit.DB;

namespace ProvingGround.PanelGenerator.Classes
{
    class PanelTesselationHelper
    {
        private Face _solidFace;

        /// <summary>
        /// Panel Tessleation Constuctor
        /// </summary>
        /// <param name="solidFace"></param>
        public PanelTesselationHelper(Face solidFace)
        {
            _solidFace = solidFace;
        }

        /// <summary>
        /// Even Quad Grid Division
        /// </summary>
        /// <param name="f">Face to tesselate</param>
        /// <param name="u">Number of divisions in the U direction</param>
        /// <param name="v">Number of divisions in the V direction</param>
        /// <returns>List of Quads</returns>
        public List<Quad> PanelQuad(Face f, int u, int v)
        {
            try
            {
                BoundingBoxUV uvBox = f.GetBoundingBox();
                UV min = uvBox.Min;
                UV max = uvBox.Max;

                List<Quad> m_quads = new List<Quad>();
                double ustep = Math.Abs(max.U-min.U) / u;
                double vstep = Math.Abs(max.V-min.V) / v;
                
                for (int i = 0; i <= u - 1; i++)
                {
                    for (int j = 0; j <= v - 1; j++)
                    {
                        XYZ ptA = f.Evaluate(new UV((i * ustep)+min.U, (j * vstep)+min.V));
                        XYZ ptB = f.Evaluate(new UV(((i + 1) * ustep) + min.U, (j * vstep)+min.V));
                        XYZ ptC = f.Evaluate(new UV(((i + 1) * ustep) + min.U, ((j + 1) * vstep)+min.V));
                        XYZ ptD = f.Evaluate(new UV((i * ustep)+min.U, ((j + 1) * vstep)+min.V));

                        Quad quad = new Quad(ptA, ptB, ptC, ptD);
                        m_quads.Add(quad);
                    }
                }
                return m_quads;
            }
            catch { return null; }
        }

        /// <summary>
        /// Even Staggerd Quad Grid Division
        /// </summary>
        /// <param name="u">Number of divisions in the U direction</param>
        /// <param name="v">Number of divisions in the V direction</param>
        /// <returns>List of Quads</returns>
        public List<Quad> PanelQuadStaggered(Face f, int u, int v)
        {
            try
            {
                BoundingBoxUV uvBox = f.GetBoundingBox();
                UV min = uvBox.Min;
                UV max = uvBox.Max;

                List<Quad> m_quads = new List<Quad>();

                double ustep = Math.Abs(max.U - min.U) / u;
                double vstep = Math.Abs(max.V - min.V) / v;

                XYZ ptA;
                XYZ ptB;
                XYZ ptC;
                XYZ ptD;

                for (int i = 0; i <= u - 1; i++)
                {
                    for (int j = 0; j <= v - 1; j++)
                    {
                        if ((j % 2) == 0)
                        {
                            ptA = f.Evaluate(new UV(i * ustep + min.U, j * vstep + min.V));
                            ptB = f.Evaluate(new UV((i + 1) * ustep + min.U, j * vstep + min.V));
                            ptC = f.Evaluate(new UV((i + 1) * ustep + min.U, (j + 1) * vstep + min.V));
                            ptD = f.Evaluate(new UV(i * ustep + min.U, (j + 1) * vstep + min.V));

                            Quad mysurface = new Quad(ptA, ptB, ptC, ptD);
                            m_quads.Add(mysurface);
                        }

                        if ((j % 2) == 1)
                        {
                            if ((i < u - 1))
                            {
                                ptA = f.Evaluate(new UV((i + 0.5) * ustep + min.U, j * vstep + min.V));
                                ptB = f.Evaluate(new UV((i + 1.5) * ustep + min.U, j * vstep + min.V));
                                ptC = f.Evaluate(new UV((i + 1.5) * ustep + min.U, (j + 1) * vstep + min.V));
                                ptD = f.Evaluate(new UV((i + 0.5) * ustep + min.U, (j + 1) * vstep + min.V));

                                Quad mysurface = new Quad(ptA, ptB, ptC, ptD);
                                m_quads.Add(mysurface);
                            }

                            if (i == 0)
                            {
                                ptA = f.Evaluate(new UV(i * ustep + min.U, j * vstep + min.V));
                                ptB = f.Evaluate(new UV((i + 0.5) * ustep + min.U, j * vstep + min.V));
                                ptC = f.Evaluate(new UV((i + 0.5) * ustep + min.U, (j + 1) * vstep + min.V));
                                ptD = f.Evaluate(new UV(i * ustep + min.U, (j + 1) * vstep + min.V));

                                Quad mysurface = new Quad(ptA, ptB, ptC, ptD);
                                m_quads.Add(mysurface);
                            }

                            if (i == v - 1)
                            {
                                ptA = f.Evaluate(new UV((i + 0.5) * ustep + min.U, j * vstep + min.V));
                                ptB = f.Evaluate(new UV((i + 1) * ustep + min.U, j * vstep + min.V));
                                ptC = f.Evaluate(new UV((i + 1) * ustep + min.U, (j + 1) * vstep + min.V));
                                ptD = f.Evaluate(new UV((i + 0.5) * ustep + min.U, (j + 1) * vstep + min.V));

                                Quad mysurface = new Quad(ptA, ptB, ptC, ptD);
                                m_quads.Add(mysurface);
                            }
                        }
                    }
                }
                return m_quads;
            }
            catch { return null; }
        }
    }



    /// <summary>
    /// Quad class
    /// </summary>
    public class Quad
    {
        private XYZ _a;
        private XYZ _b;
        private XYZ _c;
        private XYZ _d;

        /// <summary>
        /// Corner point A
        /// </summary>
        public XYZ A { get { return _a; } }

        /// <summary>
        /// Corner point B
        /// </summary>
        public XYZ B { get { return _b; } }

        /// <summary>
        /// Corner point C
        /// </summary>
        public XYZ C { get { return _c; } }

        /// <summary>
        /// Corner point D
        /// </summary>
        public XYZ D { get { return _d; } }

        /// <summary>
        /// Create new Quad
        /// </summary>
        /// <param name="a">Corner A</param>
        /// <param name="b">Corner B</param>
        /// <param name="c">Corner C</param>
        /// <param name="d">Corner D</param>
        public Quad(XYZ a, XYZ b, XYZ c, XYZ d)
        {
            _a = a;
            _b = b;
            _c = c;
            _d = d;
        }
    }
}
