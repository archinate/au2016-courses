﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;

namespace ProvingGround.PanelGenerator
{
    public class RevitCommandHelper
    {
        private ExternalCommandData _cmd;

        public string _filepath;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="cmd"></param>
        public RevitCommandHelper(ExternalCommandData cmd)
        {

            // Widen Scope
            _cmd = cmd;

        }
        public UIApplication UiApp
        {
            get
            {
                try
                {
                    return _cmd.Application;
                }
                catch { }
                return null;
            }
        }
        public Application App
        {
            get
            {
                try
                {
                    return _cmd.Application.Application;
                }
                catch { }
                return null;
            }
        }

        public UIDocument UiDoc
        {
            get
            {
                try
                {
                    return _cmd.Application.ActiveUIDocument;
                }
                catch { }
                return null;
            }
        }

        public Document ActiveDoc
        {
            get
            {
                try
                {
                    return UiDoc.Document;
                }
                catch { }
                return null;
            }
        }
    }
}