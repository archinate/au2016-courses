﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Autodesk.Revit;
using Autodesk.Revit.DB;

namespace ProvingGround.PanelGenerator.Classes
{
    /// <summary>
    /// Geometry class
    /// </summary>
    public class ElementGeometryHelper
    {
        private Element _elem;
        private Options _opt;

        private List<Solid> _solids;
        private List<Curve> _curves;
        private List<Mesh> _meshes;

        public ElementGeometryHelper(Element Elem, Options GeometryOpt)
        {
            _elem = Elem;
            _opt = GeometryOpt;

            ProcessGeometry();
        }

        #region Public Properties
        /// <summary>
        /// Unique ID
        /// </summary>
        public string UniqueID
        {
            get
            {
                return _elem.UniqueId.ToString();
            }
        }

        /// <summary>
        /// Element ID
        /// </summary>
        public string ElementID
        {
            get
            {
                return _elem.Id.ToString();
            }
        }

        /// <summary>
        /// Element solids
        /// </summary>
        public List<Solid> Solids
        {
            get
            {
                return _solids;
            }
        }

        /// <summary>
        /// Element curves
        /// </summary>
        public List<Curve> Curves
        {
            get
            {
                return _curves;
            }
        }

        /// <summary>
        /// Element meshes
        /// </summary>
        public List<Mesh> Meshes
        {
            get
            {
                return _meshes;
            }
        }
        #endregion

        #region Public Members
        public Face FindSelectedFace(XYZ pt)
        {
            Face selFace = null;
            foreach (Solid s in Solids)
            {
                foreach (Face f in s.Faces)
                {
                    IntersectionResult proj = f.Project(pt);
                    XYZ projPt = proj.XYZPoint;
                    if (projPt.DistanceTo(pt) < 0.01)
                    {
                        selFace = f;
                        break;
                    }
                }
            }
            return selFace;
        }
        #endregion

        #region Private Members - Process Geometry
        /// <summary>
        /// Process Geometry
        /// </summary>
        private void ProcessGeometry()
        {
            _solids = new List<Solid>();
            _curves = new List<Curve>();
            _meshes = new List<Mesh>();
            GeometryElement m_geo = _elem.get_Geometry(_opt);
            GetGeometryObjects(m_geo);
        }

        /// <summary>
        /// Get Geometry Objects from Element
        /// </summary>
        /// <param name="GeometryElem">Geometry Element</param>
        private void GetGeometryObjects(GeometryElement GeometryElem)
        {
            foreach (Object geo in GeometryElem)
            {
                if (geo is GeometryObject)
                {
                    GeometryObject gObj = (GeometryObject)geo;

                    // get solids
                    Solid solid = null;
                    try { solid = (Solid)gObj; }
                    catch { }
                    if (solid != null)
                    {
                        _solids.Add(solid);
                    }

                    // get curves
                    Curve curve = null;
                    try { curve = (Curve)gObj; }
                    catch { }
                    if (curve != null)
                    {
                        _curves.Add(curve);
                    }

                    // get meshes
                    Mesh mesh = null;
                    try { mesh = (Mesh)gObj; }
                    catch { }
                    if (mesh != null)
                    {
                        _meshes.Add(mesh);
                    }
                }
                if (geo is GeometryInstance)
                {
                    GeometryInstance gInst = (GeometryInstance)geo;
                    GetGeometryObjects(gInst.GetInstanceGeometry());
                }
                if (geo is GeometryElement)
                {
                    GeometryElement gElem = (GeometryElement)geo;
                    GetGeometryObjects(gElem);
                }
            }
        }
        #endregion
    }
}