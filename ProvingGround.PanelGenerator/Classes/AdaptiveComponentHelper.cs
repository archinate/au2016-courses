﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Autodesk.Revit.DB;

namespace ProvingGround.PanelGenerator.Classes
{
    class AdaptiveComponentHelper
    {
        private Document _doc;

        /// <summary>
        /// Adaptive component helper constructure
        /// </summary>
        /// <param name="doc">Revit Document</param>
        public AdaptiveComponentHelper(Document doc)
        {
            _doc = doc;
        }

        /// <summary>
        /// Create Adaptive Component Quads
        /// </summary>
        /// <param name="fs">Family Sumbol</param>
        /// <param name="quads">list of Quad point classes</param>
        /// <returns>Adaptive Components</returns>
        public List<FamilyInstance> AdaptiveComponentQuads(FamilySymbol fs, List<Quad> quads)
        {
            List<FamilyInstance> famInsts = new List<FamilyInstance>();
            using (Transaction t = new Transaction(_doc, "Create Adaptive Components"))
            {
                t.Start();
                foreach (Quad q in quads)
                {
                    FamilyInstance adptFam = AdaptiveComponentInstanceUtils.CreateAdaptiveComponentInstance(_doc, fs);
                    IList<ElementId> adptPts = AdaptiveComponentInstanceUtils.GetInstancePlacementPointElementRefIds(adptFam);

                    ReferencePoint refPt0 = (ReferencePoint)_doc.GetElement(adptPts[0]);
                    ReferencePoint refPt1 = (ReferencePoint)_doc.GetElement(adptPts[1]);
                    ReferencePoint refPt2 = (ReferencePoint)_doc.GetElement(adptPts[2]);
                    ReferencePoint refPt3 = (ReferencePoint)_doc.GetElement(adptPts[3]);

                    refPt0.Position = q.A;
                    refPt1.Position = q.B;
                    refPt2.Position = q.C;
                    refPt3.Position = q.D;

                    famInsts.Add(adptFam);
                }
                t.Commit();
            }

            return famInsts;
        }
    }
}
