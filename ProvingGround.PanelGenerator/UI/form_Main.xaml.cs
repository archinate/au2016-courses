﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;

using ProvingGround.PanelGenerator.Classes;

namespace ProvingGround.PanelGenerator
{
    /// <summary>
    /// Interaction logic for form_Main.xaml
    /// </summary>
    public partial class form_Main : Window
    {
        private RevitCommandHelper _rvtCmd;
        private Element _selectedElem;
        private Face _selectedFace;

        public form_Main(RevitCommandHelper cmdDataHelper)
        {
            InitializeComponent();

            //widen scope
            _rvtCmd = cmdDataHelper;
        }

        #region Private Members
        /// <summary>
        /// Tesselate Face with Adaptive Components
        /// </summary>
        private void TesselateFace()
        {
            try
            {
                string symbName = "QuadAdaptive-Simple";
                FamilySymbol fs = GetFamilySymbol(symbName);

                if (fs != null)
                {
                    AdaptiveComponentHelper adptComp = new AdaptiveComponentHelper(_rvtCmd.ActiveDoc);

                    PanelTesselationHelper panelTess = new PanelTesselationHelper(_selectedFace);
                    int u = Convert.ToInt16(slider_UDiv.Value);
                    int v = Convert.ToInt16(slider_VDiv.Value);
                    List<Quad> quads = panelTess.PanelQuadStaggered(_selectedFace, u, v);

                    adptComp.AdaptiveComponentQuads(fs, quads);
                }
                else
                {
                    MessageBox.Show("We could not find the Family Symbol: " + symbName, "No Family Found", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch { }
        }

        /// <summary>
        /// Get FamilySymbol
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private FamilySymbol GetFamilySymbol(string name)
        {
            FamilySymbol fs = null;
            FilteredElementCollector filtElem = new FilteredElementCollector(_rvtCmd.ActiveDoc);
            filtElem.OfClass(typeof(FamilySymbol));
            foreach (FamilySymbol f in filtElem)
            {
                if (f.Name == name)
                {
                    fs = f;
                    break;
                }
            }

            return fs;
        }
        #endregion

        #region Private Events
        /// <summary>
        /// Close the window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Close_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Run code here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Run_Click(object sender, RoutedEventArgs e)
        {
            if (_selectedElem != null)
            {
                TesselateFace();
                this.Close();
            }
            else
            {
                MessageBox.Show("No valid faces selected. Please select a valid face.", "Nothing Selected.", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        /// <summary>
        /// Drag Window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        /// <summary>
        /// Select Face
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_SelectFace_Click(object sender, RoutedEventArgs e)
        {
            Element elem = null;
            Face face = null;
            this.Hide();
            try
            {
                //Selection of face
                Selection sel = _rvtCmd.UiApp.ActiveUIDocument.Selection;
                Reference pickedRef = sel.PickObject(ObjectType.PointOnElement, "Please select a Face");
                elem = _rvtCmd.ActiveDoc.GetElement(pickedRef.ElementId);

                XYZ globalSelPt = pickedRef.GlobalPoint;

                if (elem != null)
                {
                    label_SelectedID.Content = "Selected Element ID: " + elem.Id.ToString();

                    //Geometry Options
                    Options opt = _rvtCmd.App.Create.NewGeometryOptions();
                    opt.View = _rvtCmd.ActiveDoc.ActiveView;
                    opt.ComputeReferences = true;

                    // Get face
                    ElementGeometryHelper elemHelp = new ElementGeometryHelper(elem, opt);
                    face = elemHelp.FindSelectedFace(globalSelPt);
                }
                else
                {
                    label_SelectedID.Content = "No valid elements found. Please try selecting again...";
                }
            }
            catch { }

            _selectedElem = elem;
            _selectedFace = face;

            this.ShowDialog();
        }
        #endregion
    }
}
